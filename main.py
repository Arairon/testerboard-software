import tkinter as tk
from tkinter import filedialog, StringVar
from typing import Literal
from math import floor
from pathlib import Path
from copy import deepcopy
import json


from base64 import b64decode
from zlib import decompress
from threading import Thread
import tester

VERSION = 0.1
DATA_FOLDER = "TesterBoard"

ASSETS = {
    "favicon": "eJwBIwTc+4lQTkcNChoKAAAADUlIRFIAAABAAAAAQAgGAAAAqmlx3gAAAYRpQ0NQSUNDIHByb2ZpbGUAACiRfZE9SMNQFIVPU6UiLQ4WEXHIUJ0sioo4ShWLYKG0FVp1MHnpHzRpSFJcHAXXgoM/i1UHF2ddHVwFQfAHxNnBSdFFSrwvKbSI8cHlfZz3zuG++wChUWGq2TUBqJplpOIxMZtbFQOv8GMAIapxiZl6Ir2Ygef6uoeP73dRnuV9788VUvImA3wi8RzTDYt4g3hm09I57xOHWUlSiM+JxwxqkPiR67LLb5yLDgs8M2xkUvPEYWKx2MFyB7OSoRJPE0cUVaN8IeuywnmLs1qpsVaf/IXBvLaS5jrVMOJYQgJJiJBRQxkVWIjSrpFiIkXnMQ//kONPkksmVxmMHAuoQoXk+MH/4PdszcLUpJsUjAHdL7b9MQIEdoFm3ba/j227eQL4n4Erre2vNoDZT9LrbS1yBPRtAxfXbU3eAy53gMEnXTIkR/JTCYUC8H5G35QD+m+B3jV3bq1znD4AGZrV8g1wcAiMFil73ePdPZ1z+/dOa34/n4dyuZC5gtUAAAAGYktHRAAAAO4A7h2NiaoAAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQfoAxYNHid9glg7AAAAGXRFWHRDb21tZW50AENyZWF0ZWQgd2l0aCBHSU1QV4EOFwAAAftJREFUeNrtm79LAmEYx7+veKYZHIYOxUFT4iLo1BC09Afo4uhf4C4OgogITi4u4mhbOCg4Obo5RENbk8OR9EPFtFMzepsjtbMc6p7nGZ+7e1/uwz3f9/lyPMLv90sQDhuIB3kAdqgqsFiQBSA8V1dSGY0g5nOaADAYsAiS1oCDdhvvOzt0NcAfCpEuATsMg/sABsAAGAADYAAMgGofsCJe43F0E4mtb6jqOhzn5/B4PFvfU2s0ME8ml65N4gvQIxE4CwUMh0O6JaBHo3DmcqYhWA6AFAJ6LAZXJmMKgiVFUAqB0dnZ70TQfnuLw1Zr5YNvPh8ewuEved/NDZReb/WG/T6mG77Qd2v2T08xd7u3ewrYOh3sdTpYd0pgCYDd62sY+fxaJXZuoNJm1nxuNn8MgBshBsAAGAB7gb8eRjgMpVTCZMX1hcv1JSektA6Ax2AQCAZN3+8wDOxXKhhTLAFlNoNWLGJ8eWnKFVoOgNrtYnxxYdoSWw7AUyAAW7VK1w0CwP3JCZRy2RSEfyGCR9XqWi9w12xi4vV+yr0cH8PBfQADYAAMgAEwAJpmyFIANjVD5EtASAm13aYJQEgJrVbDNJul6Qa1eh2zdJqmG9QaDcxSqY3+DgueF+BGiAEwAAZA2gvwyAyPzHAfQFoDeGSGR2Z4ZIYBMADC8QHQ5Kl8O0zKdQAAAABJRU5ErkJgggCruq0=",
}

style = {
    "bg-": "#001010",
    "bg": "#002020",
    "bg+": "#306060",
    "bg++": "#408080",
    "fg": "#aaffff",
    "hl": "#00eeee",
    "hl-": "#60aaaa",
    "select_bg": "#204080",
    "error": "#ff5050",
    "mixed_err": "#ffaa50",
    "red+": "#ff5050",
    "red": "#ff0000",
    "pin selected": "yellow",
    "pin not selected": "#60aaaa",
    "pin did not match": "#ff0000",
    "pin default color": "#306060",
    "not connected": "#ff5050",
    "ok": "#41c48b",
    "scan true": "#239875",
    "scan none": "#306060",
    "scan false": "#604040",
    "scan disabled": "#103535",
    "state disabled": "#204040",
}

hint_style = {
    "swdio": {"border": "#eaca80", "bg": "#202040"},
    "swclk": {"border": "#9090ee", "bg": "#202040"},
    "spi_mosi": {"border": "#ee6030", "bg": "#702020"},
    "spi_miso": {"border": "#5050ee", "bg": "#702020"},
    "spi_sck": {"border": "#40bb40", "bg": "#702020"},
    "uart_rx": {"border": "#5050ee", "bg": "#006050"},
    "uart_tx": {"border": "#ee6030", "bg": "#006050"},
    "gpio": {"border": "#80bbbb", "bg": "#608080"},
    "i2c_scl": {
        "border": "#ccccee",
        "bg": "#303040",
    },
    "i2c_sda": {
        "border": "#5050ee",
        "bg": "#101040",
    },
    "+5.0v": {
        "border": "#bb2020",
        "bg": "#501010",
    },
    "+4.2vbat": {
        "border": "#aa6060",
        "bg": "#601010",
    },
    "+3.3v": {
        "border": "#ff8080",
        "bg": "#701010",
    },
    "gnd": {
        "border": "#404060",
        "bg": "#202020",
    },
    "disabled": {"border": "#000000", "bg": "#002020"},
}
DISABLED_HINTS = ["spi_mosi", "spi_miso", "spi_sck", "swdio", "swclk", "gpio"]
# DISABLED_HINTS = []
# hint_style.update(dict.fromkeys(DISABLED_HINTS, None))

fontsize = {"normal": 12, "small": 10}
fonts = {
    "label": "Courier New",
    "default": "Courier New",
    "pin": "Microsoft Sans Serif",
    "menu": "tahoma",
    "mono": "Courier New",
}


def load_style_file(obj):
    global DISABLED_HINTS
    target = obj.get("SELECTED_STYLE", "main")
    if "SELECTED_STYLE" in obj:
        del obj["SELECTED_STYLE"]
    if target not in obj:
        return
    loaded_style = obj[target]
    style.update(loaded_style.get("style", {}))
    hint_style.update(loaded_style.get("hint_style", {}))
    fontsize.update(loaded_style.get("fontsize", {}))
    fonts.update(loaded_style.get("fonts", {}))
    if "disabled_hints" in loaded_style:
        DISABLED_HINTS = loaded_style.get("disabled_hints", [])


try:
    if Path(DATA_FOLDER).joinpath("styles.json").exists():
        with open("styles.json") as file:
            load_style_file(json.load(file))
    elif Path("styles.json").exists():
        with open("styles.json") as file:
            load_style_file(json.load(file))
except Exception:
    pass


def s(value):
    return style.get(value, value)


def threadrun(func, *args, d=True):
    t = Thread(target=func, args=args, daemon=d)
    t.start()


INTERFACES_INITIALIZED = False


def reset_interfaces():
    global INTERFACES_INITIALIZED
    INTERFACES_INITIALIZED = False
    i2cRead_btn.config(text="Read")
    i2cRead_btn.set_border(s("hl"))
    interfaces_init_frame.tkraise()
    interfaces_placeholder.tkraise()
    pin_frame.tkraise()


def reset():
    reset_interfaces()
    for pin in Pin.instances:
        pin.button_out.highlight_border("pin not selected")
        pin.button_in.highlight_border("pin not selected")
        pin.button_out.highlight(None)
        pin.button_in.highlight(None)
        pin.show_hint()
    if SELECTED_OUTPIN:
        SELECTED_OUTPIN.unselect()
    if SELECTED_PIN:
        SELECTED_PIN.unselect()


tester.add_callback("reset", reset)


class Frame(tk.Frame):
    instances = []

    def __init__(self, root, bg, *args, **kwargs):
        super().__init__(root, *args, **kwargs)
        self.background = bg
        self.hlcolor = "error"
        self.hlwidth = 0
        self.place_params = None
        self.manager = None
        self.hidden = False
        self.update()
        Frame.instances.append(self)

    def update(self):
        self.config(
            bg=s(self.background),
            highlightbackground=s(self.hlcolor),
            highlightthickness=self.hlwidth,
            highlightcolor=s(self.hlcolor),
        )

    def border(self, width, color):
        self.hlcolor = color
        self.hlwidth = width
        self.update()
        return self

    def debug(self, color="red"):
        self.border(1, color)
        return self

    def grid(self, **kwargs):
        self.place_params = kwargs
        self.manager = "grid"
        super().grid(**kwargs)
        return self

    def pack(self, **kwargs):
        self.place_params = kwargs
        self.manager = "pack"
        super().pack(**kwargs)
        return self

    def hide(self):
        assert self.manager, "This frame hasn't been shown for it to be hidden"
        if self.manager == "grid":
            self.grid_forget()
        else:
            self.pack_forget()
        self.hidden = True

    def show(self):
        assert self.hidden, "This frame hasn't been hidden for it to be shown"
        if self.manager == "grid":
            self.grid(**self.place_params)
        else:
            self.pack(**self.place_params)
        self.hidden = False


class Button(tk.Button):
    def __init__(self, root, *args, frameless=False, bordercolor="hl", borderwidth=1, **kwargs):
        self.style = {"bg": "bg", "fg": "fg", "font": "default"}
        self.frameless = frameless
        if not self.frameless:
            self.bordercolor = bordercolor
            self.borderwidth = borderwidth
            self.frame = Frame(root, "hl")
            self.packed = False
            super().__init__(self.frame, *args, **kwargs)
        else:
            super().__init__(root, *args, **kwargs)
        self.fontsize = "small"
        self.config(relief="flat")
        self.update()

    def set_border(self, color, width=None):
        assert not self.frameless
        if width is not None:
            self.borderwidth = width
        self.bordercolor = color
        self.update()

    def update(self):
        cfg = {
            "bg": s(self.style["bg"]),
            "fg": s(self.style["fg"]),
            "font": (fonts[self.style["font"]], fontsize[self.fontsize]),
            "activebackground": s("bg"),
            "activeforeground": s("hl"),
        }
        self.config(**cfg)
        if not self.frameless:
            self.frame.hlcolor = self.bordercolor
            self.frame.hlwidth = self.borderwidth
            self.frame.update()
        return self

    def grid(self, *args, **kwargs):
        self.update()
        if self.frameless:
            super().grid(*args, **kwargs)
            return self
        self.frame.grid(*args, **kwargs)
        if not self.packed:
            self.pack(fill="both")
            self.packed = True
        return self

    def hide(self):
        self.frame.hide()

    def show(self):
        self.frame.show()

    def grid_forget(self):
        if self.frameless:
            super().grid_forget()
        else:
            self.frame.grid_forget()
        return self


SELECTED_PIN = None


class PinInButton(Button):
    instances = []

    def __init__(self, root, pin, *args, **kwargs):
        super().__init__(root, *args, **kwargs)
        self.pin = pin
        pin.button_in = self
        self.config(text=pin.name)
        PinInButton.instances.append(self)
        self.borderwidth = 3
        self.bordercolor = "pin not selected"
        self.style["font"] = "pin"
        self.style["bg"] = "pin default color"
        self.update()
        self.config(command=self.click)

    def click(self):
        self.pin.select_in()

    def select(self):
        self.set_border("pin selected")

    def unselect(self):
        self.set_border("pin not selected")
        self.pin.show_hint()

    def color(self, color):
        self.style["bg"] = color
        self.update()

    def highlight(self, state):
        if state in [True, False, None, "disabled"]:
            self.color(f"scan {str(state).lower()}")
        else:
            self.color(state)
        self.update()

    def highlight_border(self, state):
        if state in ["disabled"]:
            self.set_border(f"state {str(state).lower()}")
        else:
            self.set_border(str(state).lower())


class PinOutButton(Button):
    instances = []

    def __init__(self, root, pin, *args, **kwargs):
        super().__init__(root, *args, **kwargs)
        self.pin = pin
        pin.button_out = self
        self.config(text=pin.name)
        PinOutButton.instances.append(self)
        self.borderwidth = 3
        self.bordercolor = "pin not selected"
        self.style["font"] = "pin"
        self.style["bg"] = "pin default color"
        self.update()
        self.config(command=self.click)

    def click(self):
        self.pin.select_out()

    def select(self):
        self.set_border("pin selected")

    def unselect(self):
        self.set_border("pin not selected")

    def highlight(self, state):
        if state in [True, False, None, "disabled"]:
            self.style["bg"] = f"scan {str(state).lower()}"
        else:
            self.style["bg"] = state
        self.update()

    def highlight_border(self, state):
        if state in ["disabled"]:
            self.set_border(f"state {str(state).lower()}")
        else:
            self.set_border(str(state).lower())


class Label(tk.Label):
    instances = []

    def __init__(self, root, *args, **kwargs):
        super().__init__(root, *args, **kwargs)
        self.config(**LABEL_DEFAULTS)
        Label.instances.append(self)

    def grid(self, **kwargs):
        super().grid(**kwargs)
        return self

    def pack(self, **kwargs):
        super().pack(**kwargs)
        return self

    def debug(self):
        self.config(relief="ridge")
        return self


class Pin:
    instances = []

    def __init__(self, name):
        self.name = name
        self.n = tester.get_number(name)
        self.button_in: PinInButton = None
        self.button_out: PinOutButton = None
        self.hint: dict = {"name": "n/a", "type": 0}
        self.report = tester.PinMap(self.name)
        self.report.clear()
        self.pos = None
        self.bus = 1 if self.name[0] == "B" else 0
        self.instances.append(self)

    def set_pos(self, pos: tuple | int, posY: int = None, *_args, bus: int = None):
        if posY is not None:
            pos = (pos, posY)
        self.pos = tuple(pos)
        if bus is not None:
            self.bus = bus
        x, y = pos
        for button in (self.button_in, self.button_out):
            button.grid_forget()
            button.grid(row=y + 1, column=x + (3 * self.bus), padx=2, pady=2)

    def reset_pos(self):
        n = (self.n - 1) % 32
        y, x = divmod(n, 2)
        self.set_pos(x, y)

    def update_name(self, name):
        raise NotImplementedError("This needs a rework. now.")
        # self.name = name
        # self.n = tester.get_number(name)
        # self.button_in.config(text=name)
        # self.button_out.config(text=name)

    def hide_hint(self):
        if not INTERFACES_INITIALIZED:
            self.button_in.set_border(style["pin not selected"])
            self.button_in.highlight(style["pin default color"])
        else:
            self.button_in.highlight_border("disabled")
            self.button_in.highlight("disabled")

    def show_hint(self):
        if not self.hint["type"]:
            self.hide_hint()
            return
        if not INTERFACES_INITIALIZED and self.hint["type"] in DISABLED_HINTS:
            self.hide_hint()
            return
        st = hint_style.get(self.hint["type"], None)
        if st:
            self.button_in.set_border(st["border"])
            self.button_in.highlight(st["bg"])

    def select_in(self):
        global SELECTED_PIN
        if INTERFACES_INITIALIZED:
            return
        if SELECTED_PIN:
            SELECTED_PIN.button_in.unselect()
        if SELECTED_PIN == self:
            if SELECTED_OUTPIN:
                SELECTED_OUTPIN.select_out()
            SELECTED_PIN = None
            for i in pinActButtons[:3]:
                i.config(state="disabled")
            highlight_vertical()
            return
        if SELECTED_OUTPIN:
            SELECTED_OUTPIN.button_out.select()
        SELECTED_PIN = self
        if SELECTED_OUTPIN:
            for i in pinActButtons[:3]:
                i.config(state="normal")
        else:
            pinActButtons[1].config(state="normal")
        self.button_in.select()
        load_pin_info(self.hint)
        outLabel.config(text=f"Output Pins [{self.name}]")
        self.highlight_reported()
        if tester_cfg["selectonclick"].get():
            tester.sel(self.n, 0)
        # set_lbl(preset_editor_selected_pin_lbl, f"Pin: {self.name}") PresetEditor

    def highlight_reported(self):
        if self != SELECTED_PIN:
            return
        for n, state in enumerate(self.report.map):
            x, y = divmod(n, 32)
            pin = pins[x][y]
            if INTERFACES_INITIALIZED:
                state = "disabled"
            pin.button_out.highlight(state)
            match_val = self.report.matchmap[n]
            if match_val is False:
                pin.button_out.set_border("pin did not match")
            elif pin is not SELECTED_OUTPIN:
                pin.button_out.set_border("pin not selected")

    def select_out(self):
        global SELECTED_OUTPIN
        if not SELECTED_PIN:
            return
        if INTERFACES_INITIALIZED:
            return
        if SELECTED_OUTPIN == self:
            SELECTED_OUTPIN.button_out.unselect()
            SELECTED_OUTPIN = None
            for i in pinActButtons[:3:2]:
                i.config(state="disabled")
            return
        elif SELECTED_OUTPIN:
            SELECTED_OUTPIN.button_out.unselect()
            SELECTED_OUTPIN = self
        else:
            SELECTED_OUTPIN = self
        # outCfgLabel.config(text=f"Output config [{self.name}]")

        for i in pinActButtons[:3]:
            i.config(state="normal")

        self.button_out.select()

        if tester_cfg["selectonclick"].get():
            tester.sel(0, self.n)

    def unselect(self, target: Literal["in", "out"] = None):
        if target == "in":
            assert self == SELECTED_PIN, f"Unselected pin[{self.name}] is not Selected pin [{SELECTED_PIN.name}]"
            self.select_in()
        elif target == "out":
            assert (
                self == SELECTED_OUTPIN
            ), f"Unselected pin[{self.name}] is not Selected outpin [{SELECTED_OUTPIN.name}]"
            self.select_out()
        else:
            assert self in [
                SELECTED_PIN,
                SELECTED_OUTPIN,
            ], f"Unselected pin[{self.name}] is neither sel_pin [{SELECTED_PIN.name}] or selout_pin [{SELECTED_OUTPIN.name}]"
            if self == SELECTED_PIN:
                self.select_in()
            else:
                self.select_out()

    def clear(self):
        self.report.clear()
        self.highlight_reported()

    def debug_scan(self):
        print(f"DEBUG SCANNING {self.name}")
        self.report = tester.debug_scan(self.name)
        if SELECTED_PIN == self:
            self.highlight_reported()


def highlight_vertical():
    if SELECTED_PIN:
        SELECTED_PIN.select_in()
        return
    outLabel.config(text="Output Pins [ | ]")
    for n, pin in enumerate(Pin.instances):
        pin.show_hint()
        v = pin.report.map[n]
        pin.button_out.highlight(v)
        pin.button_out.set_border("pin not selected")
    hide_pin_info()


SELECTED_PIN = None
SELECTED_OUTPIN = None


def rescan_com():
    ports = tester.com_scan()
    coms = list(map(lambda x: x.split(":")[0], ports))
    uart_coms = []
    for i in ports:
        if i.lower().count("uart"):
            uart_coms.append(i)
    if sel_port.get() not in coms:
        sel_port.set(ports[-1].split(":")[0])
    if len(uart_coms) == 1:
        sel_port.set(uart_coms[0].split(":")[0])
        connect_com()
    rcv_portsel["menu"].delete(0, "end")
    for i in ports:
        rcv_portsel["menu"].add_command(
            label=i,
            command=tk._setit(sel_port, i),
        )


def connect_com():
    status = tester.connect(sel_port.get())
    if status:
        tester.start_listener()
        con_status.config(text="<OK>", fg=s("ok"))
        set_lbl(curstat_out, f'▏{"Connected.": ^25}▕', s("hl"))
        con_b.config(text="disconnect", command=tester.disconnect)
        rcv_portsel["state"] = "disabled"
    else:
        set_lbl(con_status, "<ERR>", s("not connected"))


def disconnect_cb():
    log("> Disconnected <")
    set_lbl(con_status, "<N/C>", s("not connected"))
    set_lbl(curstat_out, f'▏{"Not Connected.": ^25}▕', s("error"))
    con_b.config(text="connect", command=connect_com)
    rcv_portsel["state"] = "normal"


tester.add_callback("disconnect", disconnect_cb)

LABEL_DEFAULTS = {
    "font": (fonts["label"], fontsize["normal"]),
    "bg": s("bg"),
    "fg": s("hl"),
}


def callback(msg):
    log("; ".join(map(str, msg)))
    progressbar()
    curstat_out.config(fg=s("hl"))
    match msg[0]:
        case "PIN":
            pin = Pin.instances[msg[1].n - 1]
            pin.report.map[msg[2].n - 1] = msg[3]
            pin.instances[msg[1].n - 1].highlight_reported()
        case "1MAP":
            pin_number = msg[1].n - 1
            if msg[2] == "12":
                pin = Pin.instances[pin_number]
                pin.report = msg[3]
                pin.highlight_reported()
            elif msg[2] == "13":
                for n, i in enumerate(msg[3].map):
                    Pin.instances[n].report.map[pin_number] = i
                    Pin.instances[n].highlight_reported()
        case "TMAP":
            for i in range(64):
                x, y = divmod(i, 32)
                pins[x][y].report.map[i] = msg[1].map[i]
            highlight_vertical()
        case "PROCESSING":
            progressbar(msg[2], msg[3])
            if msg[1] == "PMAP" and tester_cfg["loadasap"].get():
                pin = Pin.instances[msg[2] - 1]
                pin.report = msg[4]
                pin.highlight_reported()
        case "BIGMAP":
            progressbar(64, 64)
            for k, v in msg[1].items():
                pin = Pin.instances[k - 1]
                pin.report = v
                pin.highlight_reported()
            highlight_vertical()
        case "SENS":
            i2c_update(msg[1])
        case "SCAN":
            i2cData.delete(0.1, "end")
            txt = ["Found Sensors:"]
            if not msg[1]:
                txt.append("none")
            txt.extend(msg[1])
            i2cData.insert(0.1, "\n  ".join(txt))
            i2cData.tag_add("title", 1.0, 2.0)
        case "OK":
            progressbar(3, 3)
            # sleep(0.05)
            stat_out("Done")


tester.add_callback("msg", callback)


def set_lbl(lbl, msg, color=None):
    if color is not None:
        lbl.config(fg=style.get(color, color))
    lbl.config(text=msg)


def stat_out(msg, color=None):
    set_lbl(curstat_out, f"▏{msg: ^25}▕", color)


pbar = {"value": 0, "max": 3, "len": 25, "color": s("hl")}


def progressbar(value=None, vmax=None, plen=None, color=None):
    if value is None:
        pbar["value"] += 1
        value = pbar["value"]
    else:
        pbar["value"] = value
    if vmax:
        pbar["max"] = vmax
    if color:
        pbar["color"] = s(color)
    if plen:
        pbar["len"] = plen
    blocks = [
        "",
        "▏",
        "▎",
        "▍",
        "▋",
        "▋",
        "▊",
        "▉",
        "█",
    ]  # ["", "▏", "▎", "▍", "▌", "▋", "▊", "▉", "█"]
    vmin, vmax = 0, vmax or pbar["max"]
    lsep, rsep = "▏", "▕"
    length = pbar["len"]
    value = min(max(value, vmin), vmax)
    value = (value - vmin) / float(vmax - vmin)
    v = value * length
    x = floor(v)  # integer part
    y = v - x  # fractional part
    base = 0.125  # 0.125 = 1/8
    prec = 3
    i = int(round(base * floor(float(y) / base), prec) / base)
    bar_str = "█" * x + blocks[i]
    n = length - len(bar_str)
    bar_str = lsep + bar_str + " " * n + rsep

    set_lbl(curstat_out, bar_str, pbar["color"])


root = tk.Tk()
wX, wY = 1280, 720
# root.geometry(f"{wX}x{wY}")
root.title(f"TesterBoard Controller v{VERSION}")
root.config(bg=style["bg"])
favicon = tk.PhotoImage(format="png", data=decompress(b64decode(ASSETS["favicon"].encode())))
# icon = tk.PhotoImage(format="png", file="ico.png")
root.iconphoto(True, favicon)
# root.grid_columnconfigure(0, weight=1)
# root.grid_columnconfigure(2, weight=8)

sidebar_frame = Frame(root, bg="bg", width=wX * 0.18)  # .debug()
sidebar_frame.pack(padx=(20, 10), pady=20, anchor="n", fill="y", expand=False, side="left")
[sidebar_frame.grid_columnconfigure(i, weight=1) for i in range(3)]
sidebar_frame.grid_propagate(0)

Frame(root, "hl").pack(side="left", fill="y", anchor="n")  # vertical splitter
main_frame = Frame(root, bg="bg")  # , width=wX*0.8)  # .debug()
main_frame.pack(padx=(5, 15), pady=20, anchor="n", fill="both", expand=True, side="left")
[main_frame.grid_columnconfigure(i, weight=1) for i in [0, 2]]
# main_frame.grid_columnconfigure(1, weight=6)

""" #PRESET EDITOR INIT ---
preset_editor_tk = tk.Toplevel()
preset_editor_tk.title("Preset Editor")
# preset_editor_tk.geometry(f"{wX//2}x{wY//2}")
preset_editor_tk.protocol("WM_DELETE_WINDOW", lambda *_: [preset_editor_tk.withdraw()])
preset_editor_tk.config(bg=s("bg"))
preset_editor_tk.withdraw()
"""

Label(
    sidebar_frame,
    text="Arai's testerboard",
).grid(row=0, column=0, columnspan=3, sticky="WE", padx=(10, 15), pady=5)


sel_port = StringVar()
sel_port.trace_add(
    "write",
    lambda *args: [
        sel_port.set(sel_port.get().split(":")[0]),
        set_lbl(con_status, "<N/C>", s("not connected")),
        rcv_portsel.config(fg=s("hl")),
    ],
)
board_con_frame = (
    Frame(sidebar_frame, bg="bg").grid(row=1, column=0, columnspan=3, ipadx=5, padx=5, sticky="news").border(1, "hl")
)
[board_con_frame.grid_columnconfigure(i, weight=1) for i in range(5)]

Label(board_con_frame, text="Port: ").grid(row=0, column=0, sticky="w", padx=5, pady=10).config(font=fonts["menu"])

rcv_portsel = tk.OptionMenu(board_con_frame, sel_port, "-")
rcv_portsel.configure(
    bg=style["bg"],
    fg=style["hl"],
    disabledforeground=style["fg"],
    font=(fonts["menu"], fontsize["small"]),
    relief="flat",
    activebackground=s("bg"),
    activeforeground=s("hl"),
    width=5,
    highlightbackground=s("hl"),
    highlightthickness=0,
    borderwidth=0,
)
rcv_portsel["menu"].configure(
    bg=style["bg"],
    fg=style["fg"],
    font=(fonts["menu"], fontsize["small"]),
    activebackground=s("bg"),
    activeforeground=s("hl"),
)
rcv_portsel.grid(row=0, column=1, columnspan=3, sticky="we")
rescan_b = Button(board_con_frame, text="rescan", command=rescan_com, frameless=True)
rescan_b.style["font"] = "menu"
rescan_b.style["fg"] = "hl"
rescan_b.grid(row=0, column=4, sticky="e", padx=5)

con_status = Label(board_con_frame, text="<N/C>").grid(row=1, columnspan=3, column=0, padx=5, pady=(0, 5))
con_status.config(fg=s("not connected"))
con_b = Button(board_con_frame, text="connect", command=connect_com, frameless=True)
con_b.style["font"] = "menu"
con_b.style["fg"] = "hl"
con_b.grid(row=1, column=3, columnspan=2, sticky="we", padx=5)


presets_frame = Frame(sidebar_frame, bg="bg").grid(row=2, columnspan=3, padx=0, pady=0, sticky="news", ipadx=5)
[presets_frame.grid_columnconfigure(i, weight=1) for i in range(3)]
Frame(presets_frame, bg=style["bg"]).border(1, "hl").grid(row=0, columnspan=3, sticky="we", padx=5, pady=(20, 5))
Label(presets_frame, text="Presets").grid(row=0, columnspan=3, padx=5, pady=(20, 5))

PRESETS = {}
hint_pins = {"power": [], "i2c": [], "uart": [], "spi": [], "swd": [], "disabled": []}
sel_preset = StringVar()
presets_menu = tk.OptionMenu(presets_frame, sel_preset, "No preset")
presets_menu.configure(
    bg=style["bg"],
    fg=style["hl"],
    font=(fonts["menu"], fontsize["small"]),
    relief="flat",
    activebackground=s("bg"),
    activeforeground=s("fg"),
    width=5,
    highlightbackground=s("hl-"),
    highlightthickness=1,
    borderwidth=1,
)
presets_menu["menu"].configure(
    bg=style["bg"],
    fg=style["fg"],
    font=(fonts["default"], fontsize["small"]),
    activebackground=s("bg"),
    activeforeground=s("hl"),
)
presets_menu.grid(row=1, column=0, columnspan=3, sticky="ew", padx=5)


def scan_presets():
    global PRESETS
    PRESETS = {}
    preset_paths = list(Path(".").glob("*.json")) + list(Path(DATA_FOLDER).rglob("*.json"))
    presets_menu["menu"].delete(0, "end")
    for path in preset_paths:
        with open(path.resolve(), encoding="utf-8") as file:
            obj = json.load(file)
            obj["loaded_from"] = file.name
            if not obj.get("preset_version", 0):
                continue
            if (v := obj.get("preset_version", 0)) != VERSION:
                log(f"Alert: Preset {path.name}'s version ({v}) does not match controller's ({VERSION})")
                continue
            try:
                name = obj["title"]
                if name not in PRESETS.keys():
                    presets_menu["menu"].add_command(
                        label=name,
                        command=tk._setit(sel_preset, name),
                    )
                else:
                    log(
                        f"Preset \"{name}\" from file {PRESETS[name]['loaded_from']}\n  was overwritten by file {file.name}"
                    )
                PRESETS[name] = obj
            except KeyError as e:
                log(f"Error: Preset {path.name} is malformed ({e})")
    presets_menu["menu"].add_command(
        label="Select file...",
        command=tk._setit(sel_preset, "Select file..."),
    )
    presets_menu["menu"].add_command(
        label="No preset",
        command=tk._setit(sel_preset, "No preset"),
    )
    if sel_preset.get() not in PRESETS.keys():
        sel_preset.set("No preset")


def sel_preset_trace(*_args):
    if sel_preset.get() == "Select file...":
        if file := filedialog.askopenfile():
            obj = json.load(file)
            if not obj.get("preset_version", 0):
                log("Invalid preset.")
                sel_preset.set("No preset")
                presets_menu.config(fg=s("hl"))
                return
            if (v := obj.get("preset_version", 0)) != VERSION:
                log(f"Alert: Preset {file.name}'s version ({v}) does not match controller's ({VERSION})")
            try:
                name = obj["title"]
                if name not in PRESETS.keys():
                    presets_menu["menu"].add_command(
                        label=name,
                        command=tk._setit(sel_preset, name),
                    )
                else:
                    log(
                        f"Preset \"{name}\" from file {PRESETS[name]['loaded_from']}\n  was overwritten by file {file.name}"
                    )
                PRESETS[name] = obj
                sel_preset.set(name)
            except KeyError as e:
                log(f"Error: Preset {file.name} is malformed ({e})")
        else:
            sel_preset.set("No preset")
        presets_menu.config(fg=s("hl"))


sel_preset.trace_add("write", sel_preset_trace)

LOADED_PRESET = None


def load_preset():
    global LOADED_PRESET
    preset = PRESETS.get(sel_preset.get(), None)
    LOADED_PRESET = preset
    presets_auto_b["state"] = "normal"
    presets_validate_b["state"] = "normal"
    # preset_editor_open_b["state"] = "normal" PresetEditor
    if not preset:
        preset = {"pins": [[{"name": "n/a", "type": 0}] * 32] * 2}
        presets_auto_b["state"] = "disabled"
        presets_validate_b["state"] = "disabled"
        # preset_editor_open_b["state"] = "disabled" #PresetEditor

    pin_positions = []
    for n in range(2):
        for i in range(32):
            hint = deepcopy(preset["pins"][n][i])
            pin = pins[n][i]
            pin.report.clear_matches()
            pin.hint = hint

            if "pos" in hint:
                pin.set_pos(hint["pos"])
                del pin.hint["pos"]
            else:
                pin.reset_pos()
            pin_positions.append((pin.bus,) + pin.pos)

            if hint["type"] == "disabled":
                hint_pins["disabled"].append(pin)
                pin.button_in.config(state="disabled", text="___")
            else:
                pin.button_in.config(state="normal", text=pin.name)

            if not isinstance(hint["type"], str):
                continue

            if hint["type"] in ("+3.3v", "+4.2vbat", "+5.0v", "gnd"):
                hint_pins["power"].append(pin)
            elif hint["type"].startswith("spi"):
                hint_pins["spi"].append(pin)
            elif hint["type"].startswith("i2c"):
                hint_pins["i2c"].append(pin)
            elif hint["type"].startswith("uart"):
                hint_pins["uart"].append(pin)

    if len(pin_positions) != len(set(pin_positions)):
        overlaps = {}

        for i in Pin.instances:
            pos = (i.bus,) + i.pos
            if pin_positions.count(pos) > 1:
                if not overlaps.get(pos):
                    overlaps[pos] = []
                overlaps[pos].append(i.name)

        log("WARNING: Pin positions overlap:")
        for k, v in overlaps.items():
            log(f"{k}: {v}")

    highlight_vertical()
    if INTERFACES_INITIALIZED:
        for pin in Pin.instances:
            pin.button_out.highlight_border("disabled")
            pin.button_in.highlight_border("disabled")
            pin.button_out.highlight("disabled")
            pin.button_in.highlight("disabled")
        for pin in hint_pins["power"] + hint_pins["i2c"] + hint_pins["disabled"]:
            pin.show_hint()
    set_lbl(presets_validate_status, "[ Not checked yet ]", "fg")


def rehint_interfaces(name):
    name = name.lower()
    others = ["i2c", "uart", "spi"]
    others.remove(name)
    for l in others:
        for pin in hint_pins[l]:
            pin.hide_hint()
    for pin in hint_pins[name]:
        pin.show_hint()


def auto_test():
    log("HA. Wish it was that easy.")


def check_reports():
    preset = LOADED_PRESET
    if not preset:
        return
    report = {"impossible": 0, "same": 0, "diff": 0}
    marked_diff = []
    for n, pin in enumerate(Pin.instances):
        expected = preset["expected"][n]

        diffs = pin.report.compare_against(expected, format="checksum")
        report[diffs[0]] += 1
        if diffs[0] != "diff":
            continue
        obj = {"missing": [], "extra": []}
        for diff in diffs[1]:
            if diff["expected"]:
                obj["missing"].append(diff["pin"].title)
            else:
                obj["extra"].append(diff["pin"].title)
        marked_diff.append(pin)
        log(
            f"{pin.name}:\n  Missing: {', '.join(obj['missing']) if obj['missing'] else '[ None ]'}\n  Extra: {', '.join(obj['extra']) if obj['extra'] else '[ None ]'}"
        )
    s = "; ".join(
        filter(
            lambda x: x,
            (
                f"Skip: {report['impossible']}" if report["impossible"] else 0,
                f"OK: {report['same']}" if report["same"] else 0,
                f"Diff: {report['diff']}" if report["diff"] else 0,
            ),
        )
    )
    colors = ["ok", "mixed_err", "error"]
    clr = 0
    if report["diff"]:
        clr = 2
    if report["impossible"]:
        clr = 1
    set_lbl(presets_validate_status, f"[ {s} ]", colors[clr])
    if report["impossible"] == 0 and report["diff"] == 0:
        set_lbl(presets_validate_status, "[ Reports matched ]", "ok")
    highlight_vertical()
    for pin in marked_diff:
        pin.button_in.set_border("pin did not match")


presets_rescan_b = Button(presets_frame, text="rescan", command=scan_presets, bordercolor="hl-").grid(
    row=2, column=0, columnspan=1, sticky="ew", padx=(5, 0)
)
presets_load_b = Button(presets_frame, text="load", command=load_preset, bordercolor="hl-").grid(
    row=2, column=1, columnspan=2, sticky="ew", padx=(0, 5)
)

presets_validate_status = Label(presets_frame, text="").grid(
    row=3, column=0, columnspan=3, sticky="ew", padx=5, pady=(7, 0)
)
set_lbl(presets_validate_status, "[ Not checked yet ]", "fg")

presets_validate_b = Button(presets_frame, text="Check reports", command=check_reports, state="disabled").grid(
    row=4, column=0, columnspan=3, sticky="ew", padx=5, pady=(5, 0)
)

presets_auto_b = Button(presets_frame, text="Automatic test", command=auto_test, state="disabled").grid(
    row=5, column=0, columnspan=3, sticky="ew", padx=5, pady=5
)

Frame(sidebar_frame, bg=style["bg"]).border(1, "hl").grid(row=12, columnspan=3, sticky="we", padx=5, pady=(20, 5))
Label(sidebar_frame, text="Settings").grid(row=12, columnspan=3, padx=5, pady=(16, 5))

tester_cfg = {
    "selectonclick": tk.IntVar(value=0),
    "loadasap": tk.IntVar(value=1),
}

checkbutton_constants = {
    "activebackground": style["bg+"],
    "activeforeground": style["hl"],
    "relief": "flat",
    "font": (fonts["label"], fontsize["small"]),
    "highlightbackground": style["bg-"],
    "highlightcolor": style["bg-"],
    "selectcolor": style["bg-"],
    "justify": "left",
    "anchor": "w",
}

tk.Checkbutton(
    sidebar_frame,
    text="Select on click",
    bg=style["bg"],
    fg=style["fg"],
    variable=tester_cfg["selectonclick"],
    **checkbutton_constants,
).grid(row=13, column=0, padx=10, pady=2, sticky="we", columnspan=3)

tk.Checkbutton(
    sidebar_frame,
    text="Load asap",
    bg=style["bg"],
    fg=style["fg"],
    variable=tester_cfg["loadasap"],
    **checkbutton_constants,
).grid(row=14, column=0, padx=10, pady=2, sticky="we", columnspan=3)


# sidebar_frame.grid_propagate(0)
logFrame = Frame(sidebar_frame, bg=s("bg-")).grid(row=20, columnspan=3, padx=5, pady=5)
[logFrame.columnconfigure(i, weight=1) for i in range(3)]
[logFrame.rowconfigure(i, weight=1) for i in range(1)]

Label(logFrame, text="Logs").grid(row=0, columnspan=3, padx=0, pady=0, sticky="we")
Frame(logFrame, bg=style["bg"]).border(1, "hl").grid(row=1, columnspan=3, sticky="we", padx=0, pady=0)
logs_txt = tk.Text(
    logFrame,
    bg=s("bg-"),
    fg=s("fg"),
    font=(fonts["default"], fontsize["small"]),
    insertontime=0,
    width=25,
    height=8,
    wrap="none",
    relief="solid",
    highlightthickness=0,
)
logs_txt.grid(row=2, column=0, padx=5, pady=0, sticky="wesn", columnspan=3)
Frame(logFrame, bg=style["bg"]).border(1, "hl").grid(row=3, columnspan=3, sticky="we", padx=0, pady=0)


def log(txt):
    logs_txt.insert("end", f"{txt}\n")
    logs_txt.see("end")


tester.add_callback("log", log)

reset_btn = Button(
    sidebar_frame,
    text="< RESET >",
    command=lambda *_: tester.reset(True),  # reset_interfaces()],
    bordercolor=style["not connected"],
    borderwidth=2,
)
reset_btn.style["font"] = "menu"
reset_btn.style["fg"] = "hl"
reset_btn.grid(row=21, columnspan=3, padx=35, pady=10, sticky="we")

#
# Main window
#


input_pins_frame = Frame(
    main_frame,
    bg="bg-",
)
input_pins_frame.grid(row=1, column=0, rowspan=6, padx=(10, 3), pady=5)
Frame(input_pins_frame, "hl").grid(row=1, column=2, rowspan=32, sticky="NSEW", padx=3)
inLabel = Label(
    input_pins_frame,
    text="Input Pins",
).grid(row=0, column=0, columnspan=5, sticky="NSEW")

output_pins_frame = Frame(
    main_frame,
    bg="bg-",
)
output_pins_frame.grid(row=1, column=2, rowspan=6, padx=(3, 10), pady=5)
Frame(output_pins_frame, "hl").grid(row=1, column=2, rowspan=32, sticky="NSEW", padx=3)
outLabel = Label(
    output_pins_frame,
    text="Output Pins [---]",
).grid(row=0, column=0, columnspan=5, sticky="NSEW")


pins: list[list[Pin]] = [[None] * 32 for _ in range(2)]

for row, X in enumerate("AB"):
    for i in range(32):
        p = Pin(f'{X}{str(i+1).rjust(2,"0")}')
        y, x = divmod(i, 2)
        PinInButton(input_pins_frame, p)  # .grid(row=x + 1, column=y + (3 * row), padx=2, pady=2)
        PinOutButton(output_pins_frame, p)  # .grid(row=x + 1, column=y + (3 * row), padx=2, pady=2)
        p.set_pos(x, y)
        pins[row][i] = p

# pin_fr_W,pin_fr_H=input_pins_frame.winfo_width(),input_pins_frame.winfo_height()
# input_pins_frame.config(width=pin_fr_W, height=pin_fr_H)
# output_pins_frame.config(width=pin_fr_W, height=pin_fr_H)

centerFrame = Frame(main_frame, bg="bg-", width=wX * 0.4)
centerFrame.grid(row=1, column=1, rowspan=2, columnspan=1, sticky="NSEW", padx=10)
[centerFrame.grid_columnconfigure(i, weight=1) for i in range(6)]
centerFrame.grid_rowconfigure(1, weight=1)
curStatLabel = Label(centerFrame, text="Current status:", anchor="w", justify="left").grid(
    row=0, column=0, columnspan=1, sticky="we", ipadx=5, ipady=5
)
curstat_out = Label(centerFrame, text="[-------------------------]", anchor="e", justify="right").grid(
    row=0, column=1, columnspan=5, sticky="we", ipadx=5, ipady=5
)
progressbar(0, 3)
stat_out("Not Connected.", s("error"))


def error_callback(msg):
    stat_out(msg, s("error"))
    log(f"ERR> {msg}")


tester.add_callback("error", error_callback)


pin_frame = Frame(centerFrame, bg="bg-").grid(row=1, column=0, columnspan=6, sticky="NSEW")
pin_disabled_frame = (
    Frame(centerFrame, bg="bg").grid(row=1, column=0, columnspan=6, sticky="NSEW").border(1, "not connected")
)
pin_disabled_frame.grid_propagate(0)
Label(pin_disabled_frame, text="Pin testing disabled.").pack(padx=50, pady=(50, 5), fill="x")
Label(pin_disabled_frame, text="Reset the board to re-enable.").pack(padx=50, pady=(5, 50), fill="x")
pin_frame.tkraise()


Frame(pin_frame, "hl", height=1).grid(row=0, columnspan=7, sticky="we")
[pin_frame.grid_columnconfigure(i, weight=1) for i in range(7)]
[pin_frame.grid_rowconfigure(i, weight=1) for i in range(2, 6)]

pin_act_frame = Frame(pin_frame, "bg-", height=1).grid(row=1, columnspan=7, sticky="we", pady=5)
[pin_act_frame.grid_columnconfigure(i, weight=1) for i in range(7)]
[pin_act_frame.grid_rowconfigure(i, weight=1) for i in range(2, 6)]

tk.Label(
    pin_act_frame,
    text="Actions:",
    bg=s("bg-"),
    fg=s("fg"),
    font=(fonts["label"], fontsize["normal"]),
    anchor="w",
    justify="left",
).grid(row=1, column=0, sticky="we", padx=(10, 2))


def pbar_func(steps, func, *args, bar_start=0, bar_end=0):
    end = bar_end if bar_end else steps + 2
    progressbar(bar_start, end)
    func(*args)


pinActButtons = [
    Button(
        pin_act_frame,
        text="pXp",
        command=lambda *_: pbar_func(1, tester.chk, SELECTED_PIN.n, SELECTED_OUTPIN.n),
    ),
    Button(
        pin_act_frame,
        text="pXa",
        command=lambda *_: pbar_func(1, tester.map1, SELECTED_PIN.n),
    ),
    Button(
        pin_act_frame,
        text="aXp",
        command=lambda *_: pbar_func(1, tester.map2, SELECTED_OUTPIN.n),
    ),
    Button(pin_act_frame, text="vert.", command=lambda *_: pbar_func(1, tester.tmap)),
    Button(pin_act_frame, text="allXall", command=lambda *_: pbar_func(64, tester.mapall)),
]
for n, i in enumerate(pinActButtons):
    i.grid(row=1, column=1 + n, padx=2, pady=5, sticky="we")
    if n < 3:
        i.config(state="disabled")

Button(
    pin_act_frame,
    text="Clear",
    command=lambda *_: (
        [i.clear() for i in Pin.instances],
        highlight_vertical(),
        set_lbl(presets_validate_status, "[ Not checked yet ]", "fg"),
    ),
).grid(row=1, column=6, padx=(2, 10), pady=5, sticky="we")

pin_info_frame = Frame(pin_frame, "bg-", height=1).grid(row=2, columnspan=7, sticky="we", pady=5)
[pin_info_frame.grid_columnconfigure(i, weight=1) for i in range(7)]
[pin_info_frame.grid_rowconfigure(i, weight=1) for i in range(1, 3)]

tk.Label(
    pin_info_frame,
    text="Info:",
    fg=style["fg"],
    bg=style["bg-"],
    font=(fonts["label"], fontsize["normal"]),
    anchor="w",
    justify="left",
).grid(row=0, column=0, columnspan=1, sticky="we", padx=10, pady=0)

pin_info = {"name": "n/a", "type": 0}
pin_info_constants = {
    "fg": style["fg"],
    "bg": style["bg-"],
    "anchor": "w",
    "justify": "right",
    "width": 35,
    "font": (fonts["label"], fontsize["normal"]),  # , 'relief':'ridge'
}

pin_info_labels = {
    "name": tk.Label(pin_info_frame, **pin_info_constants),
    "type": tk.Label(pin_info_frame, **pin_info_constants),
    "comment": tk.Label(pin_info_frame, **pin_info_constants),
}
pin_info_labels["name"].grid(row=0, column=1, columnspan=6, sticky="we", padx=10)
pin_info_labels["type"].grid(row=1, column=1, columnspan=6, sticky="we", padx=10)
pin_info_labels["comment"].grid(row=2, column=1, columnspan=6, sticky="we", padx=10)

pin_info_variables = {"name": StringVar(), "type": StringVar(), "comment": StringVar()}
for k, lbl in pin_info_labels.items():
    lbl.config(textvariable=pin_info_variables[k])


def load_pin_info(hint):
    pin_info_variables["name"].set(f"Name: {hint['name'].title()}")
    pin_type = hint["type"]
    if pin_type == 0:
        pin_type = "Not specified"
    pin_info_variables["type"].set(f"Type: {pin_type.upper()}")
    if comment := hint.get("comment", None):
        pin_info_variables["comment"].set(f"Comment: {comment}")
    else:
        pin_info_variables["comment"].set("")


def hide_pin_info(text="No pin selected"):
    pin_info_variables["name"].set(text)
    pin_info_variables["type"].set("")
    pin_info_variables["comment"].set("")


# PRESET EDITOR START ---
"""
preset_editor_open_b = Button(pin_frame, text="preset editor", command=preset_editor_tk.deiconify)
# preset_editor_open_b.style['bg']='bg-'
preset_editor_open_b.set_border("hl", 1)
preset_editor_open_b.grid(row=9, column=6, columnspan=3, padx=10, pady=10, sticky="we")
preset_editor_open_b["state"] = "disabled"

# [preset_editor_tk.grid_rowconfigure(i,weight=1) for i in range(5)]

preset_editor_general_frame = Frame(preset_editor_tk, "bg").pack(
    side="left", fill="both", padx=15, pady=15, expand=True
)

Label(preset_editor_general_frame, text="<placeholder>", width=20).pack(pady=150)

Frame(preset_editor_tk, "bg").pack(side="left", fill="y").border(1, "hl")
preset_editor_pin_frame = Frame(preset_editor_tk, "bg").pack(side="left", fill="both", padx=15, pady=15, expand=True)

preset_editor_selected_pin_lbl = Label(preset_editor_pin_frame, text="Pin: N/A", anchor="w", justify="left").grid(
    row=0, column=0, columnspan=7, sticky="we", padx=25, pady=(25, 5)
)

[preset_editor_pin_frame.grid_columnconfigure(i, weight=1) for i in range(5)]

preset_editor_entries_const = {
    "bg": style["bg-"],
    "fg": style["fg"],
    "highlightbackground": style["hl-"],
    "highlightcolor": style["hl"],
    "highlightthickness": 1,
    "font": (fonts["default"], fontsize["normal"]),
    "insertbackground": style["fg"],
    "width": 15,
}

preset_editor_vars = {
    "name": StringVar(),
    "type": StringVar(),
    "comment": StringVar(),
}

preset_editor_entries = {
    "name": tk.Entry(preset_editor_pin_frame, **preset_editor_entries_const, textvariable=preset_editor_vars["name"]),
    "type": tk.OptionMenu(preset_editor_pin_frame, preset_editor_vars["type"], ""),
    # "type": tk.Entry(preset_editor_pin_frame, **preset_editor_entries_const),
    "comment": tk.Entry(
        preset_editor_pin_frame, **preset_editor_entries_const, textvariable=preset_editor_vars["comment"]
    ),
}
preset_editor_entries["type"].configure(
    bg=style["bg-"],
    fg=style["hl"],
    font=(fonts["menu"], fontsize["small"]),
    relief="flat",
    activebackground=s("bg"),
    activeforeground=s("hl"),
    width=preset_editor_entries_const["width"],
    highlightbackground=s("hl-"),
    highlightthickness=1,
    borderwidth=1,
)
preset_editor_entries["type"]["menu"].configure(
    bg=style["bg"],
    fg=style["fg"],
    font=(fonts["menu"], fontsize["small"]),
    activebackground=s("bg"),
    activeforeground=s("hl"),
)
preset_editor_entries["type"]["menu"].add_command(
    label="NOT SPECIFIED",
    command=tk._setit(preset_editor_vars["type"], ""),
)
for type in hint_style.keys():
    preset_editor_entries["type"]["menu"].add_command(
        label=type.upper(),
        command=tk._setit(preset_editor_vars["type"], type),
    )

preset_editor_label_constants = pin_info_constants
preset_editor_label_constants.update({"width": 20, "font": (fonts["label"], fontsize["small"])})
preset_editor_cur_lbls = {
    "name": Label(preset_editor_pin_frame, **pin_info_constants),
    "type": Label(preset_editor_pin_frame, **pin_info_constants),
    "comment": Label(preset_editor_pin_frame, **pin_info_constants),
}

del preset_editor_label_constants["width"]
for n, name in enumerate(preset_editor_entries.keys(), start=1):
    Label(preset_editor_pin_frame, text=name[:4].title() + ":", **preset_editor_label_constants).grid(
        row=n, column=0, sticky="we"
    )
    preset_editor_entries[name].grid(row=n, column=1, columnspan=3, sticky="we", padx=5)
    preset_editor_cur_lbls[name].grid(row=n, column=4, columnspan=3, sticky="we")
for k, lbl in preset_editor_cur_lbls.items():
    lbl.config(textvariable=pin_info_variables[k])
"""
# PRESET EDITOR END ---

Frame(pin_frame, "hl", height=1).grid(row=10, columnspan=7, sticky="wes")

interfacesFrame = Frame(main_frame, bg="bg-", width=wX * 0.4)
interfacesFrame.grid(row=3, column=1, rowspan=4, columnspan=1, sticky="nsew", padx=10, pady=(10, 10)).grid_propagate(0)
[interfacesFrame.grid_columnconfigure(i, weight=1) for i in range(6)]
interfacesFrame.grid_rowconfigure(1, weight=1)

Label(interfacesFrame, text="Interfaces:", anchor="w", justify="left").grid(
    row=0, column=0, columnspan=3, sticky="we", ipadx=5, ipady=5
)

i2cFrame = Frame(interfacesFrame, "bg").border(1, "hl")
i2cFrame.grid(row=1, column=0, sticky="nswe", columnspan=6).grid_propagate(0)
[i2cFrame.grid_columnconfigure(i, weight=1) for i in range(6)]


def i2c_toggle_read():
    tester.sens_toggle_read()
    if tester.READING_SENS:
        i2cRead_btn.config(text="Stop")
        i2cRead_btn.set_border(s("red+"))
    else:
        i2cRead_btn.config(text="Read")
        i2cRead_btn.set_border(s("hl"))


i2cButtons = Frame(i2cFrame, "bg")
i2cButtons.grid(row=2, column=0, columnspan=1, sticky="news")

[i2cButtons.grid_columnconfigure(i, weight=1) for i in range(3)]
Button(i2cButtons, text="Scan", command=tester.scan_i2c).grid(
    row=0, column=0, columnspan=3, sticky="news", padx=10, pady=5
)
Button(i2cButtons, text="Init All", command=tester.init_sens).grid(
    row=1, column=0, columnspan=3, sticky="news", padx=10, pady=5
)
# for n,i in enumerate(('bmp','lsm','lis'),start=1):
#    Button(i2cButtons, text=i, command=lambda *_: tester.init_sens(f'{n}')).grid(row=7,column=n-1,sticky='news',padx=(5 if n==1 else 0, 5 if n==2 else 0),pady=(0,5))


i2cRead_btn = Button(i2cButtons, text="Read", command=i2c_toggle_read).grid(
    row=2, column=0, columnspan=2, sticky="news", padx=(10, 0), pady=5
)
Button(
    i2cButtons,
    text="Once",
    command=lambda *_: (tester.read_sens() if not tester.READING_SENS else [tester.read_sens(), i2c_toggle_read()]),
).grid(row=2, column=2, columnspan=1, sticky="news", padx=(0, 10), pady=5)
i2cData_frame = Frame(i2cFrame, "hl")
i2cData_frame.grid(row=2, column=1, columnspan=5, sticky="news", padx=(0, 10))
i2cData = tk.Text(
    i2cData_frame,
    bg=s("bg"),
    fg=s("fg"),
    wrap="none",
    font=(fonts["mono"], fontsize["normal"]),
    relief="flat",
    insertontime=0,
    selectbackground=s("select_bg"),
    height=15,
    width=10,
)
i2cData.pack(padx=1, pady=0, fill="both")
i2cData.tag_config(
    "title",
    foreground=s("hl"),
)  # , background=s('bg-'))


def i2c_update(report: dict):
    tab = " " * 2
    lines = []
    tag_lines = []
    tag_n = 1
    # i2cData.tag_remove("title",0.1,'end')
    for sensor, data in report.items():
        lines.append(f"> {sensor} [{data['status']}]:")  # .ljust(14))
        tag_lines.append((f"{tag_n}.0", float(tag_n + 1)))
        del data["status"]
        l = []
        for k, v in data.items():
            if isinstance(v, tuple):
                v = map(lambda x: str(x).rjust(6), v)
                v = f"\n{tab*2}" + (" ".join(map(": ".join, zip("xyz", v))))
            else:
                v = str(v).rjust(7)
            l.append(f"{tab}{k.rjust(3)}: {v}")
        if l:
            lines.append("\n".join(l))
            tag_n += len(l) + sum(map(lambda x: x.count("\n"), l))
        tag_n += 1
    i2cData.delete(0.1, "end")
    i2cData.insert(0.1, "\n".join(lines))
    [i2cData.tag_add("title", *i) for i in tag_lines]


uartFrame = Frame(interfacesFrame, "bg-").border(1, "hl")
uartFrame.grid(row=1, column=0, sticky="nswe", columnspan=6).grid_propagate(0)
uartFrame.grid_columnconfigure(0, weight=1)


spiFrame = Frame(interfacesFrame, "bg-").border(1, "hl")
spiFrame.grid(row=1, column=0, sticky="nswe", columnspan=6).grid_propagate(0)
spiFrame.grid_columnconfigure(0, weight=1)

interfaces_placeholder = Frame(interfacesFrame, "bg").border(1, "not connected")
interfaces_placeholder.grid(row=1, column=0, sticky="nswe", columnspan=6).grid_propagate(0)

interfaces_warn_txt = tk.Text(
    interfaces_placeholder,
    bg=s("bg"),
    fg=s("fg"),
    font=(fonts["label"], fontsize["normal"]),
    insertontime=0,
    wrap="word",
    relief="flat",
    highlightthickness=0,
)
interfaces_warn_txt.pack(padx=25, pady=25, fill="both")
interfaces_warn_txt.insert(
    0.1,
    """
Warning: Pin testing will be unavailable after initializing interfaces.\n
You can re-enable pin testing by resetting the board.
""",
)
interfaces_warn_txt.config(state="disabled")

interfaces_sel_frame = Frame(interfacesFrame, "bg-")
interfaces_sel_frame.grid(row=0, column=3, columnspan=3, sticky="we")
[interfaces_sel_frame.grid_columnconfigure(i, weight=1) for i in range(3)]

interfaces_init_frame = Frame(interfacesFrame, "bg-")
interfaces_init_frame.grid(row=0, column=3, columnspan=3, sticky="we")
[interfaces_init_frame.grid_columnconfigure(i, weight=1) for i in range(3)]

for n, name, frame in [
    (0, "I2C", i2cFrame),
    (1, "UART", uartFrame),
    (2, "SPI", spiFrame),
]:
    lbl = Label(frame, text=f" {name}", anchor="w", justify="left").grid(
        row=0, column=0, columnspan=6, sticky="we", ipadx=15, ipady=5
    )
    com = lambda name, frame: lambda *_: [frame.tkraise(), rehint_interfaces(name)]
    btn = Button(interfaces_sel_frame, text=name, command=com(name, frame))
    btn.grid(row=0, column=n, sticky="we")


def interfaces_init():
    global INTERFACES_INITIALIZED
    INTERFACES_INITIALIZED = True
    tester.initialize_interfaces()
    interfaces_sel_frame.tkraise()
    pin_disabled_frame.tkraise()
    i2cFrame.tkraise()
    i2cData.delete(0.1, "end")
    if SELECTED_PIN:
        SELECTED_PIN.unselect()
    for pin in Pin.instances:
        pin.button_out.highlight_border("disabled")
        pin.button_in.highlight_border("disabled")
        pin.button_out.highlight("disabled")
        pin.button_in.highlight("disabled")
    for pin in hint_pins["power"] + hint_pins["i2c"] + hint_pins["disabled"]:
        pin.show_hint()
    outLabel.config(text="Output Pins [N/A]")


interfacesInit_btn = Button(interfaces_init_frame, text="Initialize interfaces", command=interfaces_init)
interfacesInit_btn.grid(row=0, column=0, columnspan=3, sticky="we")


highlight_vertical()
tester_cfg["selectonclick"].set(True)
rescan_com()
scan_presets()


def debug_all():
    [i.debug() for i in Frame.instances]
    [i.debug() for i in Label.instances]
    # [i.debug_scan() for i in Pin.instances]


def debug_func1():
    for n, i in enumerate(pins[0]):
        i.update_name(f"A{32-n:0>2}")


def debug_func2():
    pins[0][1].report = tester.PinMap(
        pins[0][1].name,
        list(map(bool, [0, 1, 0, 1] + [0] * 24 + [1, 1] + [0] * 22 + [1, 1] + [0] * 10)),
    )
    pins[0][1].highlight_reported()


def debug_func3():
    for pin in Pin.instances:
        pin.report.update_from_integer(0)
    highlight_vertical()


def debug_func4():
    pin = pins[0][1]
    pin.report.update_from_bytemap(pin.report.get_bytes())
    pin.highlight_reported()


def debug_func5():
    pin = pins[0][1]
    pin.report.update_from_integer(pin.report.get_integer())
    pin.highlight_reported()


def debug_func6():
    for i in Pin.instances:
        if checksum := i.report.get_checksum():
            log(f"{i.name}: {checksum} \n   {i.report.get_bytes()} {i.report.get_integer()}")


def debug_func7():
    for n, i in enumerate(Pin.instances):
        i.report.update_from_integer(n)
    highlight_vertical()


def stopstopstop():
    log("stopping for debug")
    log("alright.")


menu = tk.Menu()
menu.add_command(label="<stop>", command=stopstopstop)
menu.add_command(label="debug all", command=debug_all)
menu.add_command(label="D names [1]", command=debug_func1)
menu.add_command(label="D A02 [2]", command=debug_func2)
menu.add_command(label="D all0 [3]", command=debug_func3)
menu.add_command(label="D t1 [4]", command=debug_func4)
menu.add_command(label="D t2 [5]", command=debug_func5)
menu.add_command(label="D info [6]", command=debug_func6)
menu.add_command(label="D misc [7]", command=debug_func7)
root.config(menu=menu)

# [i.debug() for i in Frame.instances]
# [i.debug() for i in Label.instances]
# [i.debug_scan() for i in Pin.instances]

root.mainloop()
