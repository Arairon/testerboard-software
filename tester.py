from threading import Thread
from time import sleep
from timeit import default_timer
from typing import Literal
from random import randint
import struct
import serial
import serial.tools.list_ports

# TODO: Verify connection with timeout and handshake.


def threadrun(func, *args, d=True, name=None):
    t = Thread(target=func, args=args, daemon=d, name=name)
    t.start()


def delay_run(delay, func, *args, threadname=None):
    def f(*args):
        sleep(delay)
        func(*args)

    threadrun(f, *args, name=threadname)


def com_scan():
    ports = serial.tools.list_ports.comports()
    comports = []
    for port, desc, _hwid in sorted(ports):
        # if len(desc) > 35:
        #    desc = desc[:25] + "..." + desc[-6:]
        comports.append(f"{port}: {desc}")
    if not comports:
        comports.append("COM-: No COM ports found")
    return comports

    # rcv_comSel["menu"].delete(0, "end")
    # rcv_COM.set(comports[-1])
    # for i in comports:
    #    rcv_comSel["menu"].add_command(label=i, command=TK._setit(rcv_COM, i))


def debug_scan(pin="Z99"):
    print(pin)
    return [bool(randint(0, 1)) for i in range(64)]


socket = serial.Serial()


callbacks = {}


def callback(callback: str, *args, **kwargs):
    cb = callbacks.get(callback, None)
    if not cb:
        return False
    cb(*args, **kwargs)
    return True


def add_callback(name, func):
    assert name, "Invalid callback name"
    callbacks[name] = func


add_callback("error", print)


def disconnect():
    try:
        socket.close()
    except Exception:
        pass
    callback("disconnect")


def connect(port, rate=115200):
    global socket
    reset(board=True, silent=True)
    disconnect()
    try:
        socket = serial.Serial(port, rate, timeout=5, write_timeout=3)
        callback("log", f"< Connected to {port} >")
        send("A")
        assert socket.is_open, "Wrong device (probably)"
        return True
    except Exception as e:
        callback("error", f"CONERR: {str(e).split(':', maxsplit=1)[0][:40]}")


LISTENING = False
buf = bytearray()
msgs = []
MSGPARSER = None


def readmsg():
    global buf
    x = buf.index(b";")
    msg = bytes(buf[0:x])
    buf = buf[x + 1 :]

    if MSGPARSER is not None:
        try:
            msg = MSGPARSER(msg)
        except Exception as e:
            callback("error", f"Eception@parsing: {e}")
    if not callback("msg", msg):
        msgs.append(msg)


class PinN:
    def __init__(self, pin) -> None:
        if isinstance(pin, str):
            if pin.count(":"):
                pin = parsepin(pin)
            self.title = pin
            self.n = get_number(pin)
        else:
            self.n = pin
            c = "B" if pin > 32 else "A"
            self.title = f"{c}{pin - (32 if pin>32 else 0)}"

    def __str__(self) -> str:
        return self.title

    def __repr__(self) -> str:
        return f"{self.title} ({self.n})"


class PinMap:
    def __init__(self, pin=None, pinmap: list[bool] | None = None):
        self.nameless = True if pin is None else False
        self.pin = pin
        if pinmap:
            assert len(pinmap) == 64, f"Map length invalid. {len(pinmap)=}"
            self.map = pinmap
        else:
            self.map = [False] * 64
        self.matchmap = [None] * 64
        self.full = False
        self.full = self.is_full()

    def update_from_bytemap(self, bytemap: bytes):
        if not bytemap:
            return
        assert isinstance(bytemap, bytes), f"{bytemap} is not bytes"
        bytemap = bytemap.ljust(8, b"\x00")
        r = []
        for page in bytemap:
            if isinstance(page, bytes):
                page = int(page.hex(), 16)
            page = bin(page)[2:].zfill(8)
            for bit in page[::-1]:
                r.append(bool(int(bit)))
        self.map = r
        return self

    def update_from_checksum(self, checksum):
        if not checksum:
            return
        self.update_from_bytemap(int(checksum, 16).to_bytes(8))
        return self

    def update_from_integer(self, integer=None):
        if not integer and integer != 0:
            return
        assert isinstance(integer, int), f"Integer {integer} is not a valid integer."
        pinmap = bin(integer)[2:].zfill(64)
        self.map = list(map(lambda x: bool(int(x)), list(str(pinmap))))
        return self

    def get_bytes(self):
        if not self.is_full():
            return None
        bytemap = []
        for i in range(8):
            page = self.map[8 * i : 8 * (i + 1)][::-1]
            page = int("".join(map(lambda x: str(int(x)), page)), 2)
            bytemap.append(page.to_bytes(1))
        return b"".join(bytemap)

    def get_checksum(self):
        if not self.is_full():
            return None
        return self.get_bytes().hex()

    def get_integer(self):
        if not self.is_full():
            return
        return int("".join(map(lambda x: str(int(x)), self.map)), 2)

    def compare_against(self, value, format: Literal["checksum", "bytemap", "pinmap", "integer"]):
        if not self.is_full():
            return ("impossible",)
        format = format.lower()
        assert format in ("checksum", "bytemap", "pinmap", "integer"), f"Invalid format: {format}"
        obj = None
        if format == "bytemap":
            value = value.hex()
        elif format == "integer":
            obj = PinMap().update_from_integer(value)
        elif format == "pinmap":
            obj = PinMap(pinmap=value)

        if obj:
            value = obj.get_checksum()

        if not value:
            return ("impossible",)

        if self.get_checksum() == value:
            self.matchmap = [True] * 64
            return ("same",)
        diff = []
        #current = PinMap().update_from_checksum(checksum).map
        current = self.map
        expected = obj.map if obj else PinMap().update_from_checksum(value).map
        for i in range(64):
            cur, exp = current[i], expected[i]
            if cur != exp:
                diff.append({"pin": PinN(i + 1), "current": cur, "expected": exp})
            self.matchmap[i] = cur == exp
        if not diff:
            callback("log", f"weird shit with {self.get_checksum()} and {value}")
            return ("same",)
        return ("diff", diff)

    def is_full(self):
        if self.full:
            return True
        for i in self.map:
            if i is None:
                return False
        self.full = True
        return True

    def clear_matches(self):
        self.matchmap = [None] * 64

    def clear(self):
        self.map = [None] * 64
        self.clear_matches()
        self.full = False

    def isnull(self):
        return not any(self.map)

    def __str__(self):
        return "".join(map(lambda x: str(int(x)), self.map))

    def __repr__(self):
        return "".join(map(lambda x: str(int(x)), self.map))


def get_number(pin: str):
    return (32 * (1 if pin[0] == "B" else 0)) + int(pin[1:])


def parsepin(pin, decode=False):
    if decode:
        pin = pin.decode()
    r, br = map(int, pin.split(":"))
    br += 1
    if r >= 4:
        r -= 4
        c = "B"
    else:
        c = "A"
    return PinN(f"{c}{br+r*8}")


# parsemap function is deprecated. Use PinMap(pin).update_from_bytes(bytemap) instead


ERRCODES = {
    0: "Impossible operation",
    1: "Invalid value",
    2: "Value already set",
    3: "Unknown operation",
    255: "HardFault.",
}


def err(code: bytes):
    code = int(code.decode())
    return ERRCODES.get(code, f"Unknown code: {code}")


def set_reading_delay(value: float | int):
    global READING_DELAY
    if not (isinstance(value, float) or isinstance(value, int)):
        raise ValueError("Reading Delay must be int or float")
    READING_DELAY = value


pmap = {}
ONGOING = "pmap"
READING_SENS = False
READING_DELAY = 1
BUSY = False
SENSOR_ADDRESS = {
    118: "bmp280",  #  err 119
    106: "lsm6ds3",  # err 107
    28: "lis3mdl",  # err 30 # NEEDS VERIFICATION
    # Sensors v0.2 (UNSOPPORTED)
    #    30: 'lsm303d',
    #    24: 'lsm303dlh'
}

ERROR_ADDRESS = {
    119: "bmp_ERROR",
    107: "lsm_ERROR",
    30: "lis3_ERROR",
}

found_sensors: set = set()

"""
#TODO: mapXmap, reset, better errors, callbacks check, profiles
"""


def reset(board=False, silent=False):
    global BUSY, READING_SENS
    BUSY = False
    READING_SENS = False
    found_sensors.clear()
    try:
        if board and socket.is_open:
            reset_board()
    except Exception as e:
        if not silent:
            raise Exception from e  # pylint:disable=W0719
    callback("reset")


def reset_board():
    reset()
    send(0, "RRR")


def parsemsg(msg) -> tuple:
    global pmap, ONGOING, BUSY
    msg = msg.split(b">")
    match msg[0].decode():
        case "START":
            sleep(0.5)
            reset()
            send("A")
            return ("START",)
        case "ST":
            return ("STATUS", parsepin(msg[1], True), parsepin(msg[2], True))
        case "PIN":
            return (
                "PIN",
                parsepin(msg[1], True),
                parsepin(msg[2], True),
                bool(int(msg[3].decode())),
            )
        case "MAP":
            cause = msg.pop(1).decode()
            pin = parsepin(msg[1], True)
            return ("1MAP", pin, cause, PinMap(pin).update_from_bytemap(msg[2]))
        case "PMAP":
            assert ONGOING == "pmap" or not ONGOING, f'Operations collision "{ONGOING}" vs "pmap"'
            ONGOING = "pmap"
            pin = parsepin(msg[1], True)
            pmap[pin.n] = PinMap(pin).update_from_bytemap(msg[2])
            return ("PROCESSING", "PMAP", pin.n, 64, pmap[pin.n])
        case "TMAP":
            return ("TMAP", PinMap("0").update_from_bytemap(msg[1]))
        case "ERR":
            errtype = msg[1].decode()
            if errtype == "V":
                return ("ERR", err(msg[2]), msg[3].decode())
            else:
                return ("ERR", err(msg[1]), msg[2].decode())
        case "FIN":
            if ONGOING == "pmap":
                ONGOING = False
                o = pmap
                pmap = {}
                return ("BIGMAP", o)
        case "SENS":
            if len(msg[2]) < 28:
                if READING_SENS:
                    delay_run(READING_DELAY + 0.2, read_sens, threadname="read_sens_delay")
                return ("ERR", "Unexpected end of packet.")
            report = (
                struct.unpack("hI", msg[2][:8])
                + struct.unpack("<hhhhhhh", msg[2][8:-6])
                + struct.unpack("<hhh", msg[2][-6:])
            )
            t = int(msg[1].hex(), 16)
            sensors = ("bmp280", "lsm6ds3", "lis3mdl")  # ✓✗?
            en_sensors = dict(
                zip(
                    sensors,
                    (bool((t >> n) & 0b1) for n in range(len(sensors))),
                )
            )
            for k, v in en_sensors.items():
                if not v:  # Not enabled
                    en_sensors[k] = "✗"
                elif k not in found_sensors:
                    en_sensors[k] = "?"
                else:
                    en_sensors[k] = "✓"

            sensor_report: dict = {
                "bmp280": {
                    "status": en_sensors["bmp280"],
                    "temp": report[0],
                    "pres": report[1],
                },
                "lsm6ds3": {
                    "status": en_sensors["lsm6ds3"],
                    "gyr": report[5:8],
                    "acc": report[2:5],
                    "temp": report[8],
                },
                "lis3mdl": {
                    "status": en_sensors["lis3mdl"],
                    "mag": report[9:12],
                },
            }
            for i in found_sensors - set(sensor_report.keys()):
                sensor_report[i] = {"status": "N/A"}

            print(sensor_report)
            if READING_SENS:
                delay_run(READING_DELAY, read_sens, threadname="read_sens_delay")
            # callback('sensor_report', sensor_report)
            return ("SENS", sensor_report)
        case "SCAN":
            sensors = list(map(lambda x: SENSOR_ADDRESS.get(x, f"UNK ({x})"), msg[1]))
            found_sensors.update(sensors)
            print(sensors)
            return ("SCAN", sensors)
        case "OK":
            BUSY = False
            return ("OK",)
        case "ACK":
            BUSY = True
            return ("ACK",)
        case "RESET":
            BUSY = False
            return ("RESET",)
        case _:
            return ("UNPARSED", msg)


MSGPARSER = parsemsg


def sel(a, b):
    send(19, a, b)


def read():
    send(2)


def chk(a, b):
    send(11, a, b)


def map1(a):
    send(12, a)


def map2(a):
    send(13, a)


def mapall():
    send(14)


def map_specific(pmap: PinMap):
    raise NotImplementedError("mapXmap is not implemented in the interface")
    # send(15, pmap)


def tmap():  # map through / vertical map
    send(16)


def scan_i2c():
    send(101)


def init_sens(x=0):
    send(105, x)


def read_sens():
    send(110)


def sens_toggle_read():
    global READING_SENS
    READING_SENS = not READING_SENS
    if READING_SENS:
        read_sens()


def mapsel(map1: PinMap, map2: PinMap):
    send(15, map1.get_bytes() + map2.get_bytes())


def initialize_interfaces():
    send(100)


def toggleparser():
    global MSGPARSER
    if MSGPARSER is None:
        MSGPARSER = parsemsg
    else:
        MSGPARSER = None
    print(MSGPARSER)


def listener(force=False):
    global LISTENING
    if LISTENING and not force:
        return
    LISTENING = True
    try:
        while LISTENING:
            try:
                assert socket.is_open
            except Exception:
                sleep(0.2)
                continue
            byte = socket.read(1)
            if not byte:
                sleep(0.2)
                continue
            buf.extend(byte)
            if byte == b";":
                readmsg()
    except serial.serialutil.SerialException:
        LISTENING = False
        disconnect()
    except Exception as e:
        LISTENING = False
        raise RuntimeError from e
    LISTENING = False


def start_listener(force=False):
    threadrun(listener, force, name="Listener")


start_listener()


def send(*args):
    r = []
    for i in args:
        if isinstance(i, str):
            r.append(i.encode())
        elif isinstance(i, int):
            r.append(i.to_bytes(1, "big"))
        elif isinstance(i, bytes):
            r.append(i)
    r = b"".join(r)
    r = r.ljust(31, b"\x00") + len(r).to_bytes(1, "big")
    send_raw(r)


TIMEOUT = 0


def send_raw(msg):
    global TIMEOUT
    if not msg:
        return
    if default_timer() - TIMEOUT < 0.2:
        sleep(0.2)  # ORE protection
    try:
        assert not BUSY, "Currently busy."
        assert len(msg) == 32, f'Invalid msg length for "{msg.decode()}"'
        socket.write(msg)
        socket.flush()
    except serial.serialutil.PortNotOpenError:
        callback("error", "Not Connected.")
    except AssertionError as e:
        callback("error", str(e))
    except serial.SerialTimeoutException:
        callback("error", "Write timeout. Maybe you've connected to the wrong device?")
        disconnect()
    except Exception as e:
        callback("error", f"Exception@send: {e}")
    TIMEOUT = default_timer()


def main():
    add_callback("msg", print)
    add_callback("error", print)

    ports = com_scan()

    print(*enumerate(ports), sep="\n")

    port = input("Select COM port > ") or "2"
    port = int(port) if port.isdigit() else 2
    port = ports[port].split(":")[0]
    rate = input("Baudrate (default: 115200):") or "115200"
    rate = int(rate) if rate.isdigit() else 115200
    print(f"Connecting to {port} with {rate=}...   ", end="\n\n")

    connect(port, rate)

    while True:
        try:
            exec(input(">> "))
        except Exception as e:
            print(f"\nException: {e}\n")


if __name__ == "__main__":
    main()
