import re
import json
from sys import exit as sysexit

st1 = """
PA8/LED
GND
ADC5
ADC4
PA10/UART1_RX
PA9/UART1_TX
PA7/MOSI
PA6/MISO
+5.0V
GND
PA5/SCK
PC13/MUX_A
PB10
PB2
+4.2VBat
GND
PA1/TIM2_CH2
PA0/TIM2_CH1
PB0
PB1
PB15/SPI2_MOSI
PB14/SPI2_MISO
+3.3V
GND
0
0
0
0
0
0
0
0
PB13/SPI2_SCK
GND
PA3/UART2_RX
PC15/MUX_C
PA2/UART2_TX
PA15
PB11/TIM2_CH4
PB3
+5.0V
GND
PB8
PB9
ADC0(Mux)/PA4(noMux)
PC14/MUX_B
+4.2Vbat
GND
PA13/SWDIO
PA14/SWCLK
PB6/SCL
PB7/SDA
PB5/TIM3_CH2
PB4/TIM3_CH1
+3.3V
GND
0
0
0
0
0
0
0
0
"""


def includes(s, *args):
    for i in args:
        if i in s:
            return True
    return False


def starts_with(s: str, *args):
    for i in args:
        if s.startswith(i):
            return True
    return False


comments = {
    "uart1_rx": "GNSS_TX",
    "uart1_tx": "GNSS_RX",
    "uart2_rx": "Radio_TX",
    "uart2_tx": "Radio_RX",
}
r = [[0] * 32, [0] * 32]
for n, i in enumerate(st1.strip("\n").split("\n")):
    i = i.split("/")[-1].strip().lower()
    obj = {
        "name": i,
        "type": 0,
    }
    i = re.sub(r"spi\d", "spi", i)
    i = re.sub(r"uart\d", "uart", i)
    if i == "0":
        obj["name"] = "n/a"
        obj["type"] = "disabled"
    elif v := re.findall(r"(^p[ab]\d\d?)", i):
        obj["name"] = v[0]
        obj["type"] = "gpio"
    elif i in ("mosi", "miso", "sck"):
        obj["name"] = f"spi1_{i}"
        obj["type"] = f"spi_{i}"
    elif i in ("scl", "sda"):
        obj["type"] = f"i2c_{i}"
    elif starts_with(i, "sw", "spi", "uart"):
        obj["type"] = i
    elif i in ("gnd", "led", "+3.3v", "+5.0v", "+4.2vbat"):
        obj["type"] = i
    elif i.startswith("tim"):
        obj["type"] = "tim"
    elif i.startswith("adc"):
        obj["type"] = "adc"
    elif i.startswith("mux"):
        obj["type"] = "mux"
    else:
        print(f"unknown: {i}")
        obj["type"] = 0
    x, y = divmod(n, 32)
    if comment := comments.get(obj["name"], None):
        obj["comment"] = comment
    if x == 0 and y < 24:
        n = 23 - y
        pY, pX = divmod(n, 2)
        obj["pos"] = (pX, pY)
    r[x][y] = obj

with open("minisat.json") as file:
    obj = json.load(file)
    file.close()
    obj["pins"] = r
    if input("Are you sure? ").lower() != "y":
        sysexit()
    with open("minisat.json", "w") as file:
        json.dump(obj, file)
